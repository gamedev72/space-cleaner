using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float horizontalInput;
    private float verticalInput;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    private Vector3 direction;

    private GameObject spawnPoint;
    [SerializeField] private GameObject projectilePrefab;

    [SerializeField] private GameObject magnetRay;


    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = GameObject.Find("Spawn Point");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MovePlayer();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(projectilePrefab, spawnPoint.transform.position, projectilePrefab.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            StartCoroutine(UseMagnet());
        }
    }

    void MovePlayer()
    {

        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        //rotate the ship
        direction = transform.localEulerAngles;
        direction.y += horizontalInput * rotationSpeed * Time.deltaTime;
        transform.localEulerAngles = direction;

        //move the ship forward or backward
        transform.Translate(new Vector3(0, verticalInput * speed * Time.deltaTime));
    }

    IEnumerator UseMagnet()
    {
        magnetRay.SetActive(true);
        yield return new WaitForSeconds(2);
        magnetRay.SetActive(false);
    }
}
