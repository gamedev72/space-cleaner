using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody projectileRb;
    private GameObject player;
    [SerializeField] private float speed;
    [SerializeField] private float maxTime;

    // Start is called before the first frame update
    void Awake()
    {
        StartCoroutine(Timer());
        player = GameObject.Find("Player");
        projectileRb = GetComponent<Rigidbody>();

        transform.localEulerAngles = player.transform.localEulerAngles;
        projectileRb.AddForce((transform.position - player.transform.position) * speed, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(maxTime);
        Destroy(gameObject);
    }
}
