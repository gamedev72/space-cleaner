using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject trashPrefab;
    [SerializeField] private GameObject blackHole;
    [SerializeField] private int trashCounter;
    [SerializeField] private int trashToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        trashCounter = GameObject.FindGameObjectsWithTag("Trash").Length;

        if(trashCounter == 0)
        {
            Vector3 spawnPos;
            for (int i = 0; i < trashToSpawn; i++)
            {
                do
                {
                    spawnPos = new Vector3(Random.Range(-7.0f, 7.0f), 0.5f, Random.Range(-20.0f, 20.0f));
                } while (spawnPos.Equals(blackHole.transform.localScale));

                Instantiate(trashPrefab, spawnPos, trashPrefab.transform.rotation);
            }

            trashToSpawn++;
        }
    }
}
