using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject blackHoleCenter;
    [SerializeField] private float force;

    [SerializeField] private GameObject player;

    void Awake()
    {
        blackHoleCenter = GameObject.FindGameObjectWithTag("Black Hole Center");
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Black Hole"))
        {
            Debug.Log("collided with " + other.gameObject.name);

            transform.position = Vector3.MoveTowards(transform.position, blackHoleCenter.transform.position, force * Time.deltaTime);

            StartCoroutine(DestroyTrash());
        }

        if(other.gameObject.name == "Magnet Ray")
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, (force / 2) * Time.deltaTime);
        }
    }

    private IEnumerator DestroyTrash()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
