﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Counter::Start()
extern void Counter_Start_m722A97F6D6947030F4480E84D9E1C8BD5C9CD0EF (void);
// 0x00000002 System.Void Counter::OnTriggerEnter(UnityEngine.Collider)
extern void Counter_OnTriggerEnter_m3E9E13D676B0DD5B44FCD09A8BF5F7268C7A0C52 (void);
// 0x00000003 System.Void Counter::.ctor()
extern void Counter__ctor_m8268512C5E82F5E698855F4CC1B317750CF97558 (void);
// 0x00000004 System.Void PlayerController::Start()
extern void PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF (void);
// 0x00000005 System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0 (void);
// 0x00000006 System.Void PlayerController::MovePlayer()
extern void PlayerController_MovePlayer_m8851F676C978D72AC985DF6E15F8928A82B5D457 (void);
// 0x00000007 System.Collections.IEnumerator PlayerController::UseMagnet()
extern void PlayerController_UseMagnet_m246177B57E6DD1B6D6FEEEFD7C2EB8872EB7F990 (void);
// 0x00000008 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33 (void);
// 0x00000009 System.Void PlayerController/<UseMagnet>d__11::.ctor(System.Int32)
extern void U3CUseMagnetU3Ed__11__ctor_mE6517B3097CD82CC58E8D1C49E114B8F33C5D8B9 (void);
// 0x0000000A System.Void PlayerController/<UseMagnet>d__11::System.IDisposable.Dispose()
extern void U3CUseMagnetU3Ed__11_System_IDisposable_Dispose_m7D6832031C1D36E9E79D40CDA24C142F5BCDB078 (void);
// 0x0000000B System.Boolean PlayerController/<UseMagnet>d__11::MoveNext()
extern void U3CUseMagnetU3Ed__11_MoveNext_m2C4D9AF48068C4F886E4EB1CCEDE83CDF4122D90 (void);
// 0x0000000C System.Object PlayerController/<UseMagnet>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUseMagnetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01A7598565BD672301C932655DE4DFB7CCDA3B8F (void);
// 0x0000000D System.Void PlayerController/<UseMagnet>d__11::System.Collections.IEnumerator.Reset()
extern void U3CUseMagnetU3Ed__11_System_Collections_IEnumerator_Reset_mEE42C99B2837D5CC8E15D93046507F7258A4A370 (void);
// 0x0000000E System.Object PlayerController/<UseMagnet>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CUseMagnetU3Ed__11_System_Collections_IEnumerator_get_Current_mD86FBBE7AEB0909FD4207904E891DDF894D7709A (void);
// 0x0000000F System.Void Projectile::Awake()
extern void Projectile_Awake_m12ADD9C3E81141584C8879C9ECB35F6C89E9FEB2 (void);
// 0x00000010 System.Void Projectile::Update()
extern void Projectile_Update_m6E056CDE2DC25EDBA5DA3F4D9B9B9A69EC656737 (void);
// 0x00000011 System.Void Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m0EC06F5B4C6ABDB164EC06DD8F010D303CEA9422 (void);
// 0x00000012 System.Collections.IEnumerator Projectile::Timer()
extern void Projectile_Timer_m898121435DE007EA955A6B2350E08C7BD641C4E9 (void);
// 0x00000013 System.Void Projectile::.ctor()
extern void Projectile__ctor_m9CB69FCF9C97712F4578D9686861E9694A270A58 (void);
// 0x00000014 System.Void Projectile/<Timer>d__7::.ctor(System.Int32)
extern void U3CTimerU3Ed__7__ctor_m3D6E25EA16CE01DACDB8BE58E6CF4F95054F47E6 (void);
// 0x00000015 System.Void Projectile/<Timer>d__7::System.IDisposable.Dispose()
extern void U3CTimerU3Ed__7_System_IDisposable_Dispose_m17F85F523782A149E4469890AA6F06D95FA350A1 (void);
// 0x00000016 System.Boolean Projectile/<Timer>d__7::MoveNext()
extern void U3CTimerU3Ed__7_MoveNext_m57B05C851D346F9E8A762B79BA5DA6836C8CBD2A (void);
// 0x00000017 System.Object Projectile/<Timer>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64EFD6DFF11274C8BF5FD0D4DD33F7331F9CC26F (void);
// 0x00000018 System.Void Projectile/<Timer>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTimerU3Ed__7_System_Collections_IEnumerator_Reset_m4F6463EFA6186C549DC8B5E18E85FBB939D72EEE (void);
// 0x00000019 System.Object Projectile/<Timer>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTimerU3Ed__7_System_Collections_IEnumerator_get_Current_m283C45B14A4492F1B834CC4627FF3B9614F89CFD (void);
// 0x0000001A System.Void SpawnManager::Start()
extern void SpawnManager_Start_m65C9EA67649948222CFF4FBBF77BE2319D813DAF (void);
// 0x0000001B System.Void SpawnManager::Update()
extern void SpawnManager_Update_mD714BA3EADCC182FB7A93B9DE347D727E3A1BC53 (void);
// 0x0000001C System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_m8DD503A0FFE79FA38CF0B7F013E54D24A04D166A (void);
// 0x0000001D System.Void Trash::Awake()
extern void Trash_Awake_m32BA988C3E7F138D7B280E9F70373159BC33E742 (void);
// 0x0000001E System.Void Trash::Update()
extern void Trash_Update_m3BB651D9BFB853DC67F2792F1660A98C7814F46F (void);
// 0x0000001F System.Void Trash::OnTriggerStay(UnityEngine.Collider)
extern void Trash_OnTriggerStay_m499BB7257493A2D61C7EFB8F4D3F520486400787 (void);
// 0x00000020 System.Collections.IEnumerator Trash::DestroyTrash()
extern void Trash_DestroyTrash_m26F8BD59F09B8856544178E491629FD8E862A715 (void);
// 0x00000021 System.Void Trash::.ctor()
extern void Trash__ctor_m812035A01DF63AB71D1A8CF18600B8EF4861B4CC (void);
// 0x00000022 System.Void Trash/<DestroyTrash>d__6::.ctor(System.Int32)
extern void U3CDestroyTrashU3Ed__6__ctor_m3E2C7F0B524BECE5D712B4008A5E6A0E964EE898 (void);
// 0x00000023 System.Void Trash/<DestroyTrash>d__6::System.IDisposable.Dispose()
extern void U3CDestroyTrashU3Ed__6_System_IDisposable_Dispose_mCF4003AE1F932719CE5C6C49C8894A7AADF91D36 (void);
// 0x00000024 System.Boolean Trash/<DestroyTrash>d__6::MoveNext()
extern void U3CDestroyTrashU3Ed__6_MoveNext_m4E0F9D03B0A082F7B8EF5DDCAD8CF85CC6D11B07 (void);
// 0x00000025 System.Object Trash/<DestroyTrash>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyTrashU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2398652864DDF5331EDD70BB2DDDAC797BC9C0A5 (void);
// 0x00000026 System.Void Trash/<DestroyTrash>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDestroyTrashU3Ed__6_System_Collections_IEnumerator_Reset_m94F40320FCF53F0872E7E169E01C4E42C34A9C5C (void);
// 0x00000027 System.Object Trash/<DestroyTrash>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyTrashU3Ed__6_System_Collections_IEnumerator_get_Current_mFD99E32976AE44F1CA40D2DCF1D416F47F4E777C (void);
static Il2CppMethodPointer s_methodPointers[39] = 
{
	Counter_Start_m722A97F6D6947030F4480E84D9E1C8BD5C9CD0EF,
	Counter_OnTriggerEnter_m3E9E13D676B0DD5B44FCD09A8BF5F7268C7A0C52,
	Counter__ctor_m8268512C5E82F5E698855F4CC1B317750CF97558,
	PlayerController_Start_m1D83076E8B136A71051F2F02545EE04947D3A8CF,
	PlayerController_FixedUpdate_m6D906D8B13844542B81CC49BA19760F747CEC8C0,
	PlayerController_MovePlayer_m8851F676C978D72AC985DF6E15F8928A82B5D457,
	PlayerController_UseMagnet_m246177B57E6DD1B6D6FEEEFD7C2EB8872EB7F990,
	PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33,
	U3CUseMagnetU3Ed__11__ctor_mE6517B3097CD82CC58E8D1C49E114B8F33C5D8B9,
	U3CUseMagnetU3Ed__11_System_IDisposable_Dispose_m7D6832031C1D36E9E79D40CDA24C142F5BCDB078,
	U3CUseMagnetU3Ed__11_MoveNext_m2C4D9AF48068C4F886E4EB1CCEDE83CDF4122D90,
	U3CUseMagnetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01A7598565BD672301C932655DE4DFB7CCDA3B8F,
	U3CUseMagnetU3Ed__11_System_Collections_IEnumerator_Reset_mEE42C99B2837D5CC8E15D93046507F7258A4A370,
	U3CUseMagnetU3Ed__11_System_Collections_IEnumerator_get_Current_mD86FBBE7AEB0909FD4207904E891DDF894D7709A,
	Projectile_Awake_m12ADD9C3E81141584C8879C9ECB35F6C89E9FEB2,
	Projectile_Update_m6E056CDE2DC25EDBA5DA3F4D9B9B9A69EC656737,
	Projectile_OnCollisionEnter_m0EC06F5B4C6ABDB164EC06DD8F010D303CEA9422,
	Projectile_Timer_m898121435DE007EA955A6B2350E08C7BD641C4E9,
	Projectile__ctor_m9CB69FCF9C97712F4578D9686861E9694A270A58,
	U3CTimerU3Ed__7__ctor_m3D6E25EA16CE01DACDB8BE58E6CF4F95054F47E6,
	U3CTimerU3Ed__7_System_IDisposable_Dispose_m17F85F523782A149E4469890AA6F06D95FA350A1,
	U3CTimerU3Ed__7_MoveNext_m57B05C851D346F9E8A762B79BA5DA6836C8CBD2A,
	U3CTimerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64EFD6DFF11274C8BF5FD0D4DD33F7331F9CC26F,
	U3CTimerU3Ed__7_System_Collections_IEnumerator_Reset_m4F6463EFA6186C549DC8B5E18E85FBB939D72EEE,
	U3CTimerU3Ed__7_System_Collections_IEnumerator_get_Current_m283C45B14A4492F1B834CC4627FF3B9614F89CFD,
	SpawnManager_Start_m65C9EA67649948222CFF4FBBF77BE2319D813DAF,
	SpawnManager_Update_mD714BA3EADCC182FB7A93B9DE347D727E3A1BC53,
	SpawnManager__ctor_m8DD503A0FFE79FA38CF0B7F013E54D24A04D166A,
	Trash_Awake_m32BA988C3E7F138D7B280E9F70373159BC33E742,
	Trash_Update_m3BB651D9BFB853DC67F2792F1660A98C7814F46F,
	Trash_OnTriggerStay_m499BB7257493A2D61C7EFB8F4D3F520486400787,
	Trash_DestroyTrash_m26F8BD59F09B8856544178E491629FD8E862A715,
	Trash__ctor_m812035A01DF63AB71D1A8CF18600B8EF4861B4CC,
	U3CDestroyTrashU3Ed__6__ctor_m3E2C7F0B524BECE5D712B4008A5E6A0E964EE898,
	U3CDestroyTrashU3Ed__6_System_IDisposable_Dispose_mCF4003AE1F932719CE5C6C49C8894A7AADF91D36,
	U3CDestroyTrashU3Ed__6_MoveNext_m4E0F9D03B0A082F7B8EF5DDCAD8CF85CC6D11B07,
	U3CDestroyTrashU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2398652864DDF5331EDD70BB2DDDAC797BC9C0A5,
	U3CDestroyTrashU3Ed__6_System_Collections_IEnumerator_Reset_m94F40320FCF53F0872E7E169E01C4E42C34A9C5C,
	U3CDestroyTrashU3Ed__6_System_Collections_IEnumerator_get_Current_mFD99E32976AE44F1CA40D2DCF1D416F47F4E777C,
};
static const int32_t s_InvokerIndices[39] = 
{
	3177,
	2597,
	3177,
	3177,
	3177,
	3177,
	3096,
	3177,
	2580,
	3177,
	3040,
	3096,
	3177,
	3096,
	3177,
	3177,
	2597,
	3096,
	3177,
	2580,
	3177,
	3040,
	3096,
	3177,
	3096,
	3177,
	3177,
	3177,
	3177,
	3177,
	2597,
	3096,
	3177,
	2580,
	3177,
	3040,
	3096,
	3177,
	3096,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	39,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
